package com.example.myapplication3;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class weixinFragment extends Fragment {

    private RecyclerView recyclerView;
    private MyAdapter myAdapter;
    private List<Map<String,Object>> data;
    private Context context;

    public weixinFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_weixin, container, false);
        context=this.getActivity();
        recyclerView=view.findViewById(R.id.RecycleView);

        data=new ArrayList<Map<String,Object>>();
        String[] name = {"李侃", "柯柯", "震哥", "正正"};
        String[] num={"18119980411", "18820020517", "18320020811", "18319960318"};
        int[] picture = {R.drawable._1,R.drawable._2,R.drawable._3,R.drawable._4};

        for (int i=0;i< name.length;i++) {
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("照片", picture[i]);
            map.put("姓名",name[i]);
            map.put("电话",num[i]);
            data.add(map);
        }

        myAdapter=new MyAdapter(data,context);
        LinearLayoutManager manager=new LinearLayoutManager(context);
        manager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(myAdapter);

        return view;
    }

}
