package com.example.myapplication3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Fragment weixinfragment=new weixinFragment();//前面小写是定义Fragment的对象为我们自己定义的大写类
    private Fragment telfragment=new telFragment();//后面全部用定义的小写对象
    private Fragment findfragment=new findFragment();
    private Fragment mefragment=new meFragment();

    private FragmentManager fragmentManager;

    private LinearLayout LinearLayout1,LinearLayout2,LinearLayout3,LinearLayout4;

    private ImageButton Imgweixin, Imgtel,Imgfind,Imgme;

    private RecyclerView recyclerView;
    private List<String> data;
    private Context context;
    private  MyAdapter myadapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        LinearLayout1=findViewById(R.id.LinearLayout_weixin);
        LinearLayout2=findViewById(R.id.LinearLayout_tel);
        LinearLayout3=findViewById(R.id.LinearLayout_find);
        LinearLayout4=findViewById(R.id.LinearLayout_me);

        LinearLayout1.setOnClickListener(this);
        LinearLayout2.setOnClickListener(this);
        LinearLayout3.setOnClickListener(this);
        LinearLayout4.setOnClickListener(this);

        Imgweixin = findViewById(R.id.imageButton);
        Imgtel = findViewById(R.id.imageButton2);
        Imgfind = findViewById(R.id.imageButton3);
        Imgme = findViewById(R.id.imageButton4);

        initFragment();
        showFragment(0);
    }

    private void initFragment(){              //初始化
        fragmentManager=getFragmentManager();
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        transaction.add(R.id.id_content,weixinfragment);
        transaction.add(R.id.id_content,telfragment);
        transaction.add(R.id.id_content,findfragment);
        transaction.add(R.id.id_content,mefragment);
        transaction.commit();
    }

    private void hideFragment(FragmentTransaction transaction){
        transaction.hide(weixinfragment);
        transaction.hide(telfragment);
        transaction.hide(findfragment);
        transaction.hide(mefragment);
    }

    @Override
    public void onClick(View view) {
        resetimg();
        switch(view.getId()){
            case R.id.LinearLayout_weixin:
                showFragment(0);
                break;
            case R.id.LinearLayout_tel:
                showFragment(1);
                break;
            case R.id.LinearLayout_find:
                showFragment(2);
                break;
            case R.id.LinearLayout_me:
                showFragment(3);
                break;
            default:
                break;
        }
    }

    private void showFragment(int i) {
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        hideFragment(transaction);
        switch(i){
            case 0:
                transaction.show(weixinfragment);
                Imgweixin.setImageResource(R.drawable.weixin);
                break;
            case 1:
                transaction.show(telfragment);
                Imgtel.setImageResource(R.drawable.tel);
                break;
            case 2:
                transaction.show(findfragment);
                Imgfind.setImageResource(R.drawable.find);
                break;
            case 3:
                transaction.show(mefragment);
                Imgme.setImageResource(R.drawable.me);
                break;
            default:
                break;
        }
        transaction.commit();
    }

    private void resetimg(){
        Imgweixin.setImageResource(R.drawable.weixin_res);
        Imgtel.setImageResource(R.drawable.tel_res);
        Imgfind.setImageResource(R.drawable.find_res);
        Imgme.setImageResource(R.drawable.me_res);
    }
}